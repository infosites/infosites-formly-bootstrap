infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setWrapper( {
		name: 'panel',
		templateUrl: 'wrappers/templates/panel.html'
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setWrapper( {
		name: 'fieldset',
		templateUrl: 'wrappers/templates/fieldset.html'
	} );
} ] );
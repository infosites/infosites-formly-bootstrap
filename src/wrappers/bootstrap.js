infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setWrapper( {
		name: 'bootstrap',
		templateUrl: 'wrappers/templates/bootstrap.html',
		apiCheck: function( check ) {
			return { templateOptions: {
				label: check.string.optional,
				required: check.bool.optional,
				labelSrOnly: check.bool.optional,
				horizontal: check.bool.optional,
				noLabel:  check.bool.optional
			} };
		}
	} );
} ] );
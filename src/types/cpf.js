infositesFormlyBootstrap.config(["formlyConfigProvider", function(e) {
    e.setType( {
        name:"cpf", "extends":"input", defaultOptions: {
            validators: {
                cpf: {
                    expression: formlyCpfValidator, message: '"CPF inválido"'
                }
            }
            , templateOptions: {
                mask: "999.999.999-99"
            }
        }
        , wrapper:["bootstrap"]
    }
    )
} ] );
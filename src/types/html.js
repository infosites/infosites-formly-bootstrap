infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'html',
		templateUrl: 'types/templates/html.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'staticText',
		templateUrl: 'types/templates/staticText.html',
		wrapper: [ 'clean' ]
	} );
} ] );
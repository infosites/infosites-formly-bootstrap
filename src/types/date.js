infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'date',
		templateUrl: 'types/templates/date.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
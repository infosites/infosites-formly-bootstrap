infositesFormlyBootstrap.config(["formlyConfigProvider", function(e) {
    e.setType( {
        name:"cnpj", "extends":"input", defaultOptions: {
            validators: {
                cnpj: {
                    expression: formlyCnpjValidator, message: '"CNPJ  inválido"'
                }
            }
            , templateOptions: {
                mask: "99.999.999/9999-99"
            }
        }
        , wrapper:["bootstrap"]
    }
    )
} ] );

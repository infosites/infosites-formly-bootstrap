infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'crop',
		templateUrl: 'types/templates/crop.html',
		link: function( scope, el, attrs, ctrl ) {
			if ( !scope.model[ scope.options.key ] ) scope.model[ scope.options.key ] = {};

			var URL = window.URL || window.webkitURL;
			var cropper = $( '.crop', el );
			var ext = null;
			scope.showCropper = false;

			cropper.cropper( {
				aspectRatio: scope.options.templateOptions.width / scope.options.templateOptions.height,
				autoCrop: true,
				guides: false,
				highlight: false,
				cropBoxMovable: false,
				cropBoxResizable: false,
				dragMode: 'move',
				cropend: function() {
					updateFile();
				},
				built: function () {
					$( this ).cropper ( 'setCropBoxData', { top: 0, left: 0, width: scope.options.templateOptions.width } );
					updateFile();
				}
			} );

			var updateFile = function() {
				var dataUrl = cropper.cropper ( 'getCroppedCanvas' ).toDataURL();
				scope.$apply( function() {
					scope.model[ scope.options.key ].dataUrl = dataUrl;
					scope.model[ scope.options.key ].lastModifiedDate = new Date();
					scope.model[ scope.options.key ].name = scope.options.key + '.' + ext;
				}) ;
			};

			scope.$watch ( function() {
				return scope.model[ scope.options.key ];
			}, function ( newValue, oldValue ) {
				if ( !newValue || !newValue.name ) return;
				var extc = newValue.name.split( '.' );
				ext = extc[ extc.length - 1 ];

				cropper.one( 'built.cropper', function () {
					URL.revokeObjectURL( newValue.dataUrl );
				} ).cropper( 'reset' ).cropper( 'replace', newValue.dataUrl );

				scope.showCropper = true;
			} );

			scope.onFile = function( $event, files ) {
				var file = files[ 0 ];

				var extc = file.name.split( '.' );
				ext = extc[ extc.length - 1 ];

				var dataUrl = URL.createObjectURL( file );

				cropper.one( 'built.cropper', function () {
					URL.revokeObjectURL( dataUrl );
				} ).cropper( 'reset' ).cropper( 'replace', dataUrl );

				scope.showCropper = true;
			};
		}
	} );
} ] );
infositesFormlyBootstrap.config(["formlyConfigProvider", function(e) {
    e.setType( {
        name:"cnpjOrCpf", "extends":"input", defaultOptions: {
            validators: {
                cnpj: {
                    expression:function(e, t, n) {
                        return"cpf"==n.to.mode?formlyCpfValidator(e, t, n): formlyCnpjValidator(e, t, n)
                    }
                    , message:'"Inválido"'
                }
            }
            , templateOptions: {
                mode: "cpf"
            }
        }
        , wrapper:["bootstrap"], link:function(e, t, n, r) {
            e.$watch(function() {
				return e.to.mode
			}, function(t, n) {
				e.to.mask="cpf"==t?"999.999.999-99": "99.999.999/9999-99"
			}
		) }
    }
    )
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'textarea',
		templateUrl: 'types/templates/textarea.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'status',
		templateUrl: 'types/templates/status.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'enum',
		templateUrl: 'types/templates/enum.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
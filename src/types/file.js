infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'file',
		templateUrl: 'types/templates/file.html',
		link: function( scope, el, attrs, ctrl ) {
			scope.file = null;

            scope.$watch(function () {
                return scope.file;
            }, function ( value ) {
				scope.model[ scope.options.key ] = value;
            });
		}
	} );
} ] );
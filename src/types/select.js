infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'select',
		templateUrl: 'types/templates/select.html',
		wrapper: [ 'bootstrap' ],
		defaultOptions: function ( options ) {
			return {
				templateOptions: {
					ngOptions: "option[to.valueProp || 'value'] as option[to.labelProp || 'name'] group by option[to.groupProp || 'group'] for option in to.options track by option[to.valueProp || 'value']"
				},
				ngModelAttrs: {
					'{{to.ngOptions}}': {
						value: "ng-options"
					}
				}
			};
		},
		link: function( scope, el, attrs, ctrl ) {
			if ( !scope.to.valueProp ) scope.to.ngOptions = "option[to.labelProp || 'name'] for option in to.options";
		}
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'checkbox',
		templateUrl: 'types/templates/checkbox.html',
		wrapper: [ 'bootstrap' ],
		defaultOptions: { templateOptions: { noLabel: true } }
	  } );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'input',
		templateUrl: 'types/templates/input.html',
		wrapper: [ 'bootstrap' ],
		defaultOptions: {
			ngModelAttrs: {
				mask: {
					attribute: 'ui-mask'
				},
				maskPlaceholder: {
					attribute: 'ui-mask-placeholder'
				}
			},
			templateOptions: {
				maskPlaceholder: ''
			}
		},
		link: function( scope, el, attrs, ctrl ) {
			if ( scope.to.type == 'password' ) scope.to.obfuscate = true;
		}
	} );
} ] );
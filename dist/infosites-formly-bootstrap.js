var infositesFormlyBootstrap = angular.module ( 'infosites-formly-bootstrap', [ 'formly', 'infosites-formly-bootstrap-templates' ] );

infositesFormlyBootstrap.run( [ 'formlyConfig', function( formlyConfig ) {
	formlyConfig.extras.errorExistsAndShouldBeVisibleExpression = 'fc.$touched || form.$submitted';
} ] );

function formlyCpfValidator(viewValue, modelValue, scope) {
	var cpf = modelValue || viewValue;
	if(cpf){
		cpf = cpf.replace(/\.|\-/g, '');
		if (cpf.length != 11 ||
		cpf == "00000000000" ||
		cpf == "11111111111" ||
		cpf == "22222222222" ||
		cpf == "33333333333" ||
		cpf == "44444444444" ||
		cpf == "55555555555" ||
		cpf == "66666666666" ||
		cpf == "77777777777" ||
		cpf == "88888888888" ||
		cpf == "99999999999")
			return false;

		var add = 0;

		for (var i = 0; i < 9; i++)
			add += parseInt(cpf.charAt(i)) * (10 - i);
		var rev = 11 - (add % 11);
		if (rev == 10 || rev == 11)
			rev = 0;
		if (rev != parseInt(cpf.charAt(9)))
			return false;
		add = 0;
		for (i = 0; i < 10; i++)
			add += parseInt(cpf.charAt(i)) * (11 - i);
		rev = 11 - (add % 11);
		if (rev == 10 || rev == 11)
			rev = 0;
		if (rev != parseInt(cpf.charAt(10)))
			return false;

		return true;
	} else if (!scope.to.required) {
		return true;
	}
	return false;
}


function formlyCnpjValidator(viewValue, modelValue, scope) {
	var cnpj = modelValue || viewValue;
	if(cnpj){
		cnpj = cnpj.replace(/[^\d]+/g,'');
		if (cnpj == "00000000000000" ||
		cnpj == "11111111111111" ||
		cnpj == "22222222222222" ||
		cnpj == "33333333333333" ||
		cnpj == "44444444444444" ||
		cnpj == "55555555555555" ||
		cnpj == "66666666666666" ||
		cnpj == "77777777777777" ||
		cnpj == "88888888888888" ||
		cnpj == "99999999999999")
			return false;

		var tamanho = cnpj.length - 2
		var numeros = cnpj.substring(0,tamanho);
		var digitos = cnpj.substring(tamanho);
		var soma = 0;
		var pos = tamanho - 7;
		for (var i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;

		tamanho = tamanho + 1;
		numeros = cnpj.substring(0,tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (var i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			return false;

		return true;
	} else if (!scope.to.required) {
		return true;
	}
	return false;
}
						
						
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setWrapper( {
		name: 'bootstrap',
		templateUrl: 'wrappers/templates/bootstrap.html',
		apiCheck: function( check ) {
			return { templateOptions: {
				label: check.string.optional,
				required: check.bool.optional,
				labelSrOnly: check.bool.optional,
				horizontal: check.bool.optional,
				noLabel:  check.bool.optional
			} };
		}
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setWrapper( {
		name: 'clean',
		templateUrl: 'wrappers/templates/clean.html'
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setWrapper( {
		name: 'fieldset',
		templateUrl: 'wrappers/templates/fieldset.html'
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setWrapper( {
		name: 'panel',
		templateUrl: 'wrappers/templates/panel.html'
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'checkbox',
		templateUrl: 'types/templates/checkbox.html',
		wrapper: [ 'bootstrap' ],
		defaultOptions: { templateOptions: { noLabel: true } }
	  } );
} ] );
infositesFormlyBootstrap.config(["formlyConfigProvider", function(e) {
    e.setType( {
        name:"cnpj", "extends":"input", defaultOptions: {
            validators: {
                cnpj: {
                    expression: formlyCnpjValidator, message: '"CNPJ  inválido"'
                }
            }
            , templateOptions: {
                mask: "99.999.999/9999-99"
            }
        }
        , wrapper:["bootstrap"]
    }
    )
} ] );

infositesFormlyBootstrap.config(["formlyConfigProvider", function(e) {
    e.setType( {
        name:"cnpjOrCpf", "extends":"input", defaultOptions: {
            validators: {
                cnpj: {
                    expression:function(e, t, n) {
                        return"cpf"==n.to.mode?formlyCpfValidator(e, t, n): formlyCnpjValidator(e, t, n)
                    }
                    , message:'"Inválido"'
                }
            }
            , templateOptions: {
                mode: "cpf"
            }
        }
        , wrapper:["bootstrap"], link:function(e, t, n, r) {
            e.$watch(function() {
				return e.to.mode
			}, function(t, n) {
				e.to.mask="cpf"==t?"999.999.999-99": "99.999.999/9999-99"
			}
		) }
    }
    )
} ] );
infositesFormlyBootstrap.config(["formlyConfigProvider", function(e) {
    e.setType( {
        name:"cpf", "extends":"input", defaultOptions: {
            validators: {
                cpf: {
                    expression: formlyCpfValidator, message: '"CPF inválido"'
                }
            }
            , templateOptions: {
                mask: "999.999.999-99"
            }
        }
        , wrapper:["bootstrap"]
    }
    )
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'crop',
		templateUrl: 'types/templates/crop.html',
		link: function( scope, el, attrs, ctrl ) {
			if ( !scope.model[ scope.options.key ] ) scope.model[ scope.options.key ] = {};

			var URL = window.URL || window.webkitURL;
			var cropper = $( '.crop', el );
			var ext = null;
			scope.showCropper = false;

			cropper.cropper( {
				aspectRatio: scope.options.templateOptions.width / scope.options.templateOptions.height,
				autoCrop: true,
				guides: false,
				highlight: false,
				cropBoxMovable: false,
				cropBoxResizable: false,
				dragMode: 'move',
				cropend: function() {
					updateFile();
				},
				built: function () {
					$( this ).cropper ( 'setCropBoxData', { top: 0, left: 0, width: scope.options.templateOptions.width } );
					updateFile();
				}
			} );

			var updateFile = function() {
				var dataUrl = cropper.cropper ( 'getCroppedCanvas' ).toDataURL();
				scope.$apply( function() {
					scope.model[ scope.options.key ].dataUrl = dataUrl;
					scope.model[ scope.options.key ].lastModifiedDate = new Date();
					scope.model[ scope.options.key ].name = scope.options.key + '.' + ext;
				}) ;
			};

			scope.$watch ( function() {
				return scope.model[ scope.options.key ];
			}, function ( newValue, oldValue ) {
				if ( !newValue || !newValue.name ) return;
				var extc = newValue.name.split( '.' );
				ext = extc[ extc.length - 1 ];

				cropper.one( 'built.cropper', function () {
					URL.revokeObjectURL( newValue.dataUrl );
				} ).cropper( 'reset' ).cropper( 'replace', newValue.dataUrl );

				scope.showCropper = true;
			} );

			scope.onFile = function( $event, files ) {
				var file = files[ 0 ];

				var extc = file.name.split( '.' );
				ext = extc[ extc.length - 1 ];

				var dataUrl = URL.createObjectURL( file );

				cropper.one( 'built.cropper', function () {
					URL.revokeObjectURL( dataUrl );
				} ).cropper( 'reset' ).cropper( 'replace', dataUrl );

				scope.showCropper = true;
			};
		}
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'date',
		templateUrl: 'types/templates/date.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'enum',
		templateUrl: 'types/templates/enum.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'file',
		templateUrl: 'types/templates/file.html',
		link: function( scope, el, attrs, ctrl ) {
			scope.file = null;

            scope.$watch(function () {
                return scope.file;
            }, function ( value ) {
				scope.model[ scope.options.key ] = value;
            });
		}
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'html',
		templateUrl: 'types/templates/html.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'input',
		templateUrl: 'types/templates/input.html',
		wrapper: [ 'bootstrap' ],
		defaultOptions: {
			ngModelAttrs: {
				mask: {
					attribute: 'ui-mask'
				},
				maskPlaceholder: {
					attribute: 'ui-mask-placeholder'
				}
			},
			templateOptions: {
				maskPlaceholder: ''
			}
		},
		link: function( scope, el, attrs, ctrl ) {
			if ( scope.to.type == 'password' ) scope.to.obfuscate = true;
		}
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'matchField',
		apiCheck: function() {
			return {
				data: {
					fieldToMatch: formlyExampleApiCheck.string
				}
			}
		},
		apiCheckOptions: {
			prefix: 'matchField type'
		},
		defaultOptions: function matchFieldDefaultOptions(options) {
			return {
				extras: {
					validateOnModelChange: true
				},
				expressionProperties: {
					'templateOptions.disabled': function(viewValue, modelValue, scope) {
						var matchField = find(scope.fields, 'key', options.data.fieldToMatch);
						if (!matchField) {
							throw new Error('Could not find a field for the key ' + options.data.fieldToMatch);
						}
						var model = options.data.modelToMatch || scope.model;
						var originalValue = model[options.data.fieldToMatch];
						var invalidOriginal = matchField.formControl && matchField.formControl.$invalid;
						return !originalValue || invalidOriginal;
					}
				},
				validators: {
					fieldMatch: {
						expression: function(viewValue, modelValue, fieldScope) {
							var value = modelValue || viewValue;
							var model = options.data.modelToMatch || fieldScope.model;
							return value === model[options.data.fieldToMatch];
						},
						message: options.data.matchFieldMessage || '"Must match"'
					}
				}
			};

			function find(array, prop, value) {
				var foundItem;
				array.some(function(item) {
					if (item[prop] === value) {
						foundItem = item;
					}
					return !!foundItem;
				});
				return foundItem;
			}
		}
	});
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'multiCheckbox',
		templateUrl: 'types/templates/multiCheckbox.html',
		wrapper: [ 'bootstrap' ],
		apiCheck: function( check ) {
			return { templateOptions: {
				options: check.arrayOf(check.object),
				labelProp: check.string.optional,
				valueProp: check.string.optional
			} };
		},
		defaultOptions: {
			noFormControl: false,
			ngModelAttrs: {
				required: {
					attribute: '',
					bound: ''
				}
			}
		},
		controller: [ '$scope', function($scope) {
			const to = $scope.to;
			const opts = $scope.options;
			$scope.multiCheckbox = {
				checked: [],
				change: setModel
			};

			// initialize the checkboxes check property
			$scope.$watch('model', function modelWatcher(newModelValue) {
				var modelValue, valueProp;

				if (Object.keys(newModelValue).length) {
					modelValue = newModelValue[opts.key];

					$scope.$watch('to.options', function optionsWatcher(newOptionsValues) {
						if (newOptionsValues && Array.isArray(newOptionsValues) && Array.isArray(modelValue)) {
							valueProp = to.valueProp || 'value';
							for (var index = 0; index < newOptionsValues.length; index++) {
								var matches = $.grep( modelValue, function( item ) {
									return item[ valueProp ] == newOptionsValues[index][valueProp];
								} );
								$scope.multiCheckbox.checked[index] = matches.length > 0;
							}
						}
					});
				}
			}, true);

			function checkValidity(expressionValue) {
				var valid;

				if ($scope.to.required) {
					valid = angular.isArray($scope.model[opts.key]) &&
						$scope.model[opts.key].length > 0 &&
						expressionValue;

					$scope.fc.$setValidity('required', valid);
				}
			}

			function setModel() {
				$scope.model[opts.key] = [];
				angular.forEach($scope.multiCheckbox.checked, function ( checkbox, index ) {
					if (checkbox) {
						$scope.model[opts.key].push(to.options[index]);
					}
				});

				// Must make sure we mark as touched because only the last checkbox due to a bug in angular.
				$scope.fc.$setTouched();
				checkValidity(true);

				if ($scope.to.onChange) {
					$scope.to.onChange();
				}
			}

			if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
				$scope.$watch(function() {
					return $scope.to.required;
				}, function(newValue) {
					checkValidity(newValue);
				});
			}

			if ($scope.to.required) {
				var unwatchFormControl = $scope.$watch('fc', function(newValue) {
					if (!newValue) {
						return;
					}
					checkValidity(true);
					unwatchFormControl();
				});
			}
		} ]
	} );
} ] );

infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'select',
		templateUrl: 'types/templates/select.html',
		wrapper: [ 'bootstrap' ],
		defaultOptions: function ( options ) {
			return {
				templateOptions: {
					ngOptions: "option[to.valueProp || 'value'] as option[to.labelProp || 'name'] group by option[to.groupProp || 'group'] for option in to.options track by option[to.valueProp || 'value']"
				},
				ngModelAttrs: {
					'{{to.ngOptions}}': {
						value: "ng-options"
					}
				}
			};
		},
		link: function( scope, el, attrs, ctrl ) {
			if ( !scope.to.valueProp ) scope.to.ngOptions = "option[to.labelProp || 'name'] for option in to.options";
		}
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'staticText',
		templateUrl: 'types/templates/staticText.html',
		wrapper: [ 'clean' ]
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'status',
		templateUrl: 'types/templates/status.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );
infositesFormlyBootstrap.config ( [ 'formlyConfigProvider', function ( formlyConfigProvider ) {
	formlyConfigProvider.setType( {
		name: 'textarea',
		templateUrl: 'types/templates/textarea.html',
		wrapper: [ 'bootstrap' ]
	} );
} ] );